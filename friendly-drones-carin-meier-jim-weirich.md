# Friendly Drones with Carin Meier & Jim Weirich #

## Listen ##

Listen to Carin Meier and Jim Weirich discuss flying drones under programmatic control with Clojure and Ruby,
the perils of flying too close to walls, and flying with goals and beliefs.

<iframe src="http://archive.org/embed/FlyingRobotsWithCarinMeierAndJimWeirich" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

[Download MP3](https://archive.org/download/FlyingRobotsWithCarinMeierAndJimWeirich/Flying%20Robots%20with%20Carin%20Meier%20and%20Jim%20Weirich.mp3)

## Watch: ##

### Flying with Argus: ###
<iframe width="640" height="390" src="http://www.youtube.com/embed/jlKt2Ed-Y04" frameborder="0" allowfullscreen></iframe>

### Following the Oriented Rondel: ###
<iframe width="640" height="390" src="http://www.youtube.com/embed/YfIZBxR9ER0" frameborder="0" allowfullscreen></iframe>

## Notes ##

### Links ###
- [Parrot AR Drone](http://ardrone2.parrot.com/usa/)
- Jim's Ruby Library: [Argus](https://github.com/jimweirich/argus)
- Carin's Clojure Library: [clj-drone](https://github.com/gigasquid/clj-drone)
- [Zurich Minds: Feedback Control](http://www.youtube.com/watch?v=C4IJXAVXgIo)

In September, we will be hosting Summer Of Drones in Cincinnati.
[Sign up](http://bit.ly/XtOfyu) to be notified by email or follow [Neo Cincinnati](http://twitter.com/neo_cincy) on Twitter to be notified when more details are available.

### Picks ###
- [Artoo Library](http://artoo.io/)
- [AR Drone Developer's guide](https://projects.ardrone.org/wiki/ardrone-api/Developer_Guide)

### See also ###
[The Joys of Flying AR Drones with Clojure](http://gigasquidsoftware.com/wordpress/?p=645)

### Participants ###
- [Carin Meier](http://twitter.com/carinmeier)
- [Jim Weirich](http://twitter.com/jimweirich)
- [Paul Henrich](http://twitter.com/p9k)

Intro music by fellow Neo human, [Mike Danko](http://twitter.com/mikedanko)

## Have feedback? ##
This is the first episode from the Cincinnati Office of Neo.
We welcome your constructive feedback: email [Paul](mailto:example.com) or send a tweet.
